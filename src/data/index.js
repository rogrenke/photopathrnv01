import atacama from './atacama.json';
import vietnam from './vietnam.json';
import australia from './australia.json';

const maps = [
  atacama,
  vietnam,
  australia,
];

export { maps };
