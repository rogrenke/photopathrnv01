import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from './common';

class Footer extends React.Component {
  render() {
    return (
      <View style={styles.headerStyle}>
        <Button onPress={() => this.props.selectPrevMarker()}>
          {'< Prev Stop'}
        </Button>
        <Button onPress={() => this.props.hideSelectedMarkerCallout()}>
          {'Hide Card'}
        </Button>
        <Button onPress={() => this.props.selectNextMarker()}>
          {'Next Stop >'}
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    height: 40,
    padding: 2,
  },
});

export default Footer;
