import React from 'react';
import { View, Text, Image, TouchableWithoutFeedback, Dimensions } from 'react-native';
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';
import Header from './Header';


class MarkerDetailView extends React.Component {

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    const imageUriObject = { uri: this.props.marker.image_large };

    return (
      <View style={styles.mainStyle}>
        <Header />
        <View style={styles.boxStyle}>
          <View style={styles.innerBoxStyle}>
            <Text style={styles.headerTextStyle}>
              {this.props.marker.title}
            </Text>
            <TouchableWithoutFeedback onPress={() => Actions.ImageDetailView({ marker: this.props.marker })}>
              <Image
                source={imageUriObject}
                style={styles.imageStyle}
              />
            </TouchableWithoutFeedback>
            <View style={styles.descriptionBlockStyle}>
              <Text style={styles.descriptionTextStyle}>
                {this.props.marker.description}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  mainStyle: {
    flex: 1
  },
  boxStyle: {
    flex: 1,
    backgroundColor: '#F4F4F4'
  },
  innerBoxStyle: {
    padding: 0
  },
  headerTextStyle: {
    width: 300,
    fontWeight: 'bold',
    fontSize: 24,
    color: '#414141',
    padding: 10
  },
  descriptionBlockStyle: {
    padding: 10
  },
  imageStyle: {
    width: Dimensions.get('window').width,
    height: 250
  },

};

export default MarkerDetailView;
