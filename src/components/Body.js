import React from 'react';
import { View, StyleSheet } from 'react-native';
import Map from './Map';

class Body extends React.Component {

  render() {
    return (
      <View style={styles.bodyStyle}>
        <Map {...this.props} ref={'map'} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bodyStyle: {
    flex: 1
  }
});

export default Body;
