import React from 'react';
import MapView from 'react-native-maps';
import Callout from './Callout.js';
import NumberMarker from './NumberMarker';

class Markers extends React.Component {
  renderMarkers() {
    return this.props.markers.map((marker, index) =>
      <MapView.Marker
        key={index}
        ref={index}
        coordinate={{
          latitude: marker.latitude,
          longitude: marker.longitude
        }}
        anchor={{ x: 0.5, y: 0.8 }}
        centerOffset={{ x: 0, y: -10 }}
        onPress={e => this.props.updateSelectedMarker(index)}
      >
        <NumberMarker markerNumber={index} />
        <Callout marker={marker} />
      </MapView.Marker>
    );
  }

  render() {
    return this.renderMarkers();
  }
}

export default Markers;
