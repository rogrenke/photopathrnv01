import React from 'react';
import { StyleSheet } from 'react-native';
import MapView from 'react-native-maps';
import Markers from './Markers';
import Directions from './Directions';

class Map extends React.Component {

  render() {
    return (
      <MapView
        style={styles.mapStyle}
        initialRegion={this.props.initialMapRegion}
      >
        <Markers {...this.props} ref={'markers'} />
        <Directions markers={this.props.markers} />
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  mapStyle: {
    ...StyleSheet.absoluteFillObject,
  }
});

export default Map;
