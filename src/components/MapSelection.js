import React from 'react';
import { View } from 'react-native';
import Orientation from 'react-native-orientation';
import { maps } from '../data';
import Header from './Header';
import SelectionBody from './SelectionBody';


class MapView extends React.Component {
  state = { mapData: [] };

  componentWillMount() {
    Orientation.lockToPortrait();
    this.setState({ mapData: maps });
  }

  render() {
    return (
      <View style={styles.mainStyle}>
        <Header initialScreen />
        <SelectionBody mapData={this.state.mapData} />
      </View>
    );
  }
}

const styles = {
  mainStyle: {
    flex: 1
  }
};

export default MapView;
