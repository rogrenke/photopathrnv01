import React from 'react';
import { View, Text, Image, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection } from './common';

class SelectionBody extends React.Component {
  renderMapItem({ item: map, index }) {
    return (
      <Card
        key={index}
        onPress={() => Actions.mapView({ mapData: map })}
      >
       <CardSection style={{ flex: 1 }}>
         <View>
           <Image
             style={styles.mapThumbnail}
             source={{ uri: map.markers[0].image_small }}
           />
         </View>
       </CardSection>
       <CardSection style={{ flex: 2, alignItems: 'center' }}>
         <View>
           <Text style={styles.mapTitle}>
             {map.title}
           </Text>
         </View>
       </CardSection>
     </Card>
    );
  }

  render() {
    return (
      <View style={styles.bodyStyle}>
        <FlatList
          data={this.props.mapData}
          renderItem={this.renderMapItem}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = {
  bodyStyle: {
    flex: 1,
    marginTop: 10,
  },
  mapThumbnail: {
    height: 80,
    width: 80
  },
  mapTitle: {
    fontFamily: 'Lobster Two',
    fontSize: 30,
    color: '#000',
  }
};

export default SelectionBody;
