import React from 'react';
import { Text, View, Image, TouchableWithoutFeedback, Dimensions } from 'react-native';
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';

class ImageDetailView extends React.Component {
  componentWillMount() {
    Orientation.lockToLandscape();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  render() {
    const imageUriObject = { uri: this.props.marker.image_large };

    return (
      <TouchableWithoutFeedback onPress={() => Actions.pop()}>
        <View style={styles.containerStyle}>
          <View style={styles.topStripStyle}>
            <Text style={styles.cancelXStyle}>
              X
            </Text>
          </View>
          <View style={styles.contentStripStyle}>
            <Image
              style={styles.imageStyle}
              source={imageUriObject}
            />
            <View>
              <Text style={styles.headerTextStyle}>
                {this.props.marker.title}
              </Text>
              <Text style={styles.descriptionTextStyle}>
                {this.props.marker.description}
              </Text>
            </View>
          </View>
          <View style={styles.bottomStripStyle}></View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const win = Dimensions.get('window');
const pictureHeight = win.width * (83 / 100);
const pictureWidth = 600 * (pictureHeight / 400);

const styles = {
  imageStyle: {
    height: pictureHeight,
    width: pictureWidth
  },
  topStripStyle: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  cancelXStyle: {
    fontSize: 24,
    color: '#E3E3E3',
    paddingRight: 10
  },
  contentStripStyle: {
    flexDirection: 'row',
    flex: 83,
    backgroundColor: '#F4F4F4'
  },
  bottomStripStyle: {
    flex: 9
  },
  headerTextStyle: {
    width: 180,
    fontWeight: 'bold',
    fontSize: 17,
    color: '#414141',
    padding: 10
  },
  descriptionTextStyle: {
    width: 180,
    fontSize: 12,
    color: '#414141',
    paddingLeft: 10
  },
  containerStyle: {
    flex: 1,
    backgroundColor: 'black',
  }
};

export default ImageDetailView;
