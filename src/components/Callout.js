import React from 'react';
import { Text, Image, View, TouchableWithoutFeedback } from 'react-native';
import MapView from 'react-native-maps';
import { Actions } from 'react-native-router-flux';

class Callout extends React.Component {

  render() {
    const imageUriObject = { uri: this.props.marker.image_small };

    return (
      <MapView.Callout>
        <TouchableWithoutFeedback onPress={() => Actions.MarkerDetailView({ marker: this.props.marker })}>
          <View
            style={styles.cardInteriorBoxStyle}
          >
            <Text style={styles.headerTextStyle} >
              {this.props.marker.title}
            </Text>
            <Image
              style={styles.imageStyle}
              source={imageUriObject}
            />
            <View style={styles.descriptionBlockStyle}>
              <Text style={styles.descriptionTextStyle}>
                {this.props.marker.description}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </MapView.Callout>
    );
  }
}

const styles = {
  cardInteriorBoxStyle: {
    backgroundColor: '#F4F4F4',
    width: 320,
    paddingLeft: 10,
    paddingBottom: 10
  },
  headerTextStyle: {
    width: 300,
    fontWeight: 'bold',
    fontSize: 24,
    color: '#414141',
  },
  imageStyle: {
    height: 200,
    width: 300
  },
  descriptionBlockStyle: {
    paddingTop: 10
  },
  descriptionTextStyle: {
    width: 300,
    color: '#414141',
  }
};

export default Callout;
