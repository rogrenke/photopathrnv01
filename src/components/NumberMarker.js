import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

class NumberMarker extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bubble}>
          <View style={styles.innerBubble}>
            <Text style={styles.number}>{this.props.markerNumber + 1}</Text>
          </View>
        </View>
        <View style={styles.arrowBorder} />
        <View style={styles.arrow} />
</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubble: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF5A5F',
    width: 24,
    height: 24,
    borderRadius: 50,
  },
  innerBubble: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 16,
    height: 16,
    borderRadius: 50,
    backgroundColor: '#fff',
  },
  number: {
    color: '#000',
    fontSize: 13,
    textAlign: 'center'
  },
  arrow: {
    zIndex: 1,
    backgroundColor: 'transparent',
    borderWidth: 9,
    borderColor: 'transparent',
    borderTopColor: '#FF5A5F',
    alignSelf: 'center',
    marginTop: -14,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderWidth: 5,
    borderColor: 'transparent',
    alignSelf: 'center',
  },
});

export default NumberMarker;
