import React from 'react';
import { View } from 'react-native';
import Orientation from 'react-native-orientation';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';


class MapView extends React.Component {
  state = { markers: [], selectedMarker: null }

  componentWillMount() {
    this.setState({ markers: this.props.mapData.markers, selectedMarker: null });
    Orientation.lockToPortrait();
  }

  componentDidUpdate() {
    this.showSelectedMarkerCallout();
  }

  selectNextMarker() {
    if (this.state.selectedMarker !== null && this.state.selectedMarker < this.state.markers.length - 1) {
      this.setState({
        selectedMarker: this.state.selectedMarker + 1
      });
    } else {
      this.setState({
        selectedMarker: 0
      });
    }
  }

  selectPrevMarker() {
    if (!this.state.selectedMarker) {
      this.setState({
        selectedMarker: this.state.markers.length - 1
      });
    } else {
      this.setState({
        selectedMarker: this.state.selectedMarker - 1
      });
    }
  }

  updateSelectedMarker(index) {
    this.setState({
      selectedMarker: index
    });
  }

  showSelectedMarkerCallout() {
    const markers = this.refs.body.refs.map.refs.markers.refs;
    markers[this.state.selectedMarker].showCallout();
  }

  hideSelectedMarkerCallout() {
    const markers = this.refs.body.refs.map.refs.markers.refs;
    if (this.state.selectedMarker) {
      markers[this.state.selectedMarker].hideCallout();
    }
  }

  render() {
    return (
      <View style={styles.mainStyle}>
        <Header style={{ flex: 1 }} />
        <Body
          style={{ flex: 3 }}
          markers={this.state.markers}
          initialMapRegion={this.props.mapData.initialMapRegion}
          ref={'body'}
          updateSelectedMarker={this.updateSelectedMarker.bind(this)}
        />
        <Footer
          style={{ flex: 1 }}
          {...this.state}
          selectNextMarker={this.selectNextMarker.bind(this)}
          selectPrevMarker={this.selectPrevMarker.bind(this)}
          hideSelectedMarkerCallout={this.hideSelectedMarkerCallout.bind(this)}
        />
      </View>
    );
  }
}

const styles = {
  mainStyle: {
    flex: 1,
    justifyContent: 'space-between'
  }
};

export default MapView;
