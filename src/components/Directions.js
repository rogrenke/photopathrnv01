import React from 'react';
import MapViewDirections from 'react-native-maps-directions';

const GOOGLE_MAPS_DIRECTION_APIKEY = 'AIzaSyBjrQeTbOiThFzAKgT4_sA0hSWrRPRZolw';

class Directions extends React.Component {

  renderDirections(arr) {
    const directions = [];
    for (let i = 0; i < arr.length - 1; i++) {
      directions.push(
        <MapViewDirections
          key={`${arr[i].latitude},${arr[i + 1].latitude}`}
          origin={{
            latitude: arr[i].latitude,
            longitude: arr[i].longitude
          }}
          destination={{
            latitude: arr[i + 1].latitude,
            longitude: arr[i + 1].longitude
          }}
          apikey={GOOGLE_MAPS_DIRECTION_APIKEY}
          mode="walking"
          strokeColor="#00B3FD"
          strokeWidth={3}
        />
      );
    }
    return directions;
  }

  render() {
    return this.renderDirections(this.props.markers);
 }
}

export default Directions;
