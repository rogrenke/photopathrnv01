import React from 'react';
import { TouchableOpacity } from 'react-native';

const Card = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={styles.containerStyle}
    >
      {props.children}
    </TouchableOpacity>
  );
};

const styles = {
  containerStyle: {
    flexDirection: 'row',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  }
};

export { Card };
