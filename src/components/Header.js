import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Header extends React.Component{
  renderBackButton() {
    if(this.props.initialScreen) {
      return (
        <Text
        style={styles.backTextStyleNotActive}
        onPress={() => Actions.pop()}
        >
          Back
        </Text>
      );
    } else {
      return (
        <Text
          style={styles.backTextStyle}
          onPress={() => Actions.pop()}
        >
          Back
        </Text>
      );
    }
  }

  render(){
    return (
      <View style={styles.headerStyle}>
        {this.renderBackButton()}
        <Text style={styles.textStyle}>Photopath</Text>
        <Text></Text>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFE',
    height: 60,
    paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2
  },
  textStyle: {
    fontFamily: 'Lobster Two',
    fontSize: 30,
    color: '#000',
    textAlign: 'center',
    flex: 1,
    paddingRight: 55
  },
  backTextStyle: {
    paddingTop: 7,
    fontSize: 20,
    paddingLeft: 10,
    textAlign: 'center'
  },
  backTextStyleNotActive: {
    paddingTop: 7,
    fontSize: 20,
    paddingLeft: 10,
    textAlign: 'center',
    color: '#FFFFFE'
  }
});

export default Header;
