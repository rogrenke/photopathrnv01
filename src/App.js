import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import ImageDetailView from './components/ImageDetailView';
import MapView from './components/MapView';
import MapSelection from './components/MapSelection';
import MarkerDetailView from './components/MarkerDetailView';


class App extends React.Component {
  render() {
    return (
      <Router>
        <Scene key="root">

          <Scene
            key="mapSelection"
            component={MapSelection}
            hideNavBar={true}
            initial
          />

          <Scene
            key="mapView"
            component={MapView}
            hideNavBar={true}
          />

          <Scene
            key="MarkerDetailView"
            component={MarkerDetailView}
            hideNavBar={true}
          />

          <Scene
            key="ImageDetailView"
            component={ImageDetailView}
            hideNavBar={true}
          />

        </Scene>
      </Router>
    );
  }
}

export default App;
